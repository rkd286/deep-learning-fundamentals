import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.linear_model import LogisticRegression


dataset = datasets.load_iris()
X = dataset.data
y = dataset.target.reshape((X.shape[0], 1))
y=y.ravel()
# print(y)
assert(X.shape[0] == y.shape[0])

n_features = X.shape[1]
n_obs = X.shape[0]
hidden_nodes = 5
n_classes = 3

# Initialize Weights
W1 = np.random.randn(n_features, hidden_nodes)
b1 = np.zeros(hidden_nodes)
W2 = np.random.randn(hidden_nodes, n_classes)
b2 = np.zeros(n_classes)

# print(X.shape)
# print(weights.shape)
# print(z.shape)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

def softmax(a):
    expA = np.exp(a)
    return expA/expA.sum(axis=1, keepdims=True)

def feed_forward(X, W1, b1, W2, b2):
    # Z = sigmoid(X.dot(W1) + b1)
    Z = np.tanh(X.dot(W1) + b1)
    A = Z.dot(W2) + b2

    #Softmax
    return softmax(A)

def classification_rate(y, P):
    n_correct = 0
    n_total = len(y)

    for i in range(n_total):
        if(y[i] == P[i]):
            n_correct += 1
    
    return (n_correct/n_total)


P_y_given_X = feed_forward(X, W1, b1, W2, b2)
P = np.argmax(P_y_given_X, axis=1)

assert(len(P) == len(y))

print("Classification Rate: {}".format(classification_rate(y, P)))