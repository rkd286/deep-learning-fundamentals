from sklearn import datasets
from sklearn.utils import shuffle
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score

def main():
    dataset = datasets.load_iris()
    X = dataset.data
    y = dataset.target.reshape((X.shape[0], 1))
    y=y.ravel()

    X,y = shuffle(X, y)
    
    n_features = X.shape[1]
    n_obs = X.shape[0]
    hidden_nodes = 6
    n_classes = 3

    model = MLPClassifier(hidden_layer_sizes=(20, 20), max_iter=1000)
    model.fit(X,y)
    predictions = model.predict(X)
    print(accuracy_score(y, predictions))


if __name__=='__main__':
    main()