import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('AgeVsExp.csv')

X = df['YearsExperience'].values.reshape(30,1)
X = np.append(X, np.ones((30,1)), axis=1)
Y = df['Salary'].values.reshape(30,1)
print(X.shape, Y.shape)
w = np.linalg.solve(X.T.dot(X), X.T.dot(Y))
print(w)


costs = []
w_grad = np.random.rand(2,1) / 1000
learning_rate = 0.001
for i in range(2048):
    y_hat = X.dot(w_grad)
    delta = y_hat - Y
    w_grad = w_grad - (learning_rate*X.T.dot(delta))
    costs.append(w_grad)

plt.scatter(df['YearsExperience'], df['Salary'])
plt.plot(df['YearsExperience'], y_hat, 'r')
plt.show()

print(w_grad)