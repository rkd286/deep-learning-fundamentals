import warnings
warnings.filterwarnings('ignore')


import tensorflow as tf
import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.utils import shuffle


def main():
    df = pd.read_csv('census_data.csv')
    df['age_category'] = pd.Series(df['age'].apply(age_category))
    
    X = df[['workclass', 'education', 'marital_status',
       'occupation', 'relationship', 'race', 'gender', 
       'hours_per_week', 'native_country',
       'age_category']]
    
    X = pd.get_dummies(X, drop_first=True).values
    y = df['income_bracket'].apply(lambda x: 0 if '<' in x else 1).values

    n_features = X.shape[1]
    n_obs = X.shape[0]
    n_classes = 2

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

    X_train_len = len(X_train)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    

    # T_test = np.zeros((len(X_test), n_classes))
    # for i in range(len(X_test)):
    #     T_test[i, y_test[i]] = 1
    

    model_lr = LogisticRegression()
    model_lr.fit(X_train, y_train)
    pred = model_lr.predict(X_test)
    print(accuracy_score(y_test, pred))


    n_hidden_layer_1 = 4
    n_hidden_layer_2 = 4
    n_hidden_layer_3 = 4
    n_hidden_layer_4 = 4
    n_hidden_layer_5 = 4
    n_hidden_layer_6 = 4
   

    learning_rate = 0.0001
    beta = 0.01
    batch_size = 32
    n_batches = n_obs // batch_size
    epochs = 100

    W1_init = np.random.randn(n_features, n_hidden_layer_1) * np.sqrt(2/(n_features + n_hidden_layer_1))
    b1_init = np.zeros(n_hidden_layer_1)
    W2_init = np.random.randn(n_hidden_layer_1, n_hidden_layer_2) * np.sqrt(2/(n_hidden_layer_1 + n_hidden_layer_2))
    b2_init = np.zeros(n_hidden_layer_2)
    W3_init = np.random.randn(n_hidden_layer_2, n_hidden_layer_3) * np.sqrt(2/(n_hidden_layer_2 + n_hidden_layer_3))
    b3_init = np.zeros(n_hidden_layer_3)
    W4_init = np.random.randn(n_hidden_layer_3, n_hidden_layer_4) * np.sqrt(2/(n_hidden_layer_3 + n_hidden_layer_4))
    b4_init = np.zeros(n_hidden_layer_4)
    W5_init = np.random.randn(n_hidden_layer_4, n_hidden_layer_5) * np.sqrt(2/(n_hidden_layer_4 + n_hidden_layer_5))
    b5_init = np.zeros(n_hidden_layer_5)
    W6_init = np.random.randn(n_hidden_layer_5, n_classes)
    b6_init = np.zeros(n_classes)

    #Variables and Placeholders
    X_ph = tf.placeholder(tf.float32, shape=(None, n_features), name='X_ph')
    y_ph = tf.placeholder(tf.float32, shape=(None, n_classes), name='y_ph')

    W1 = tf.Variable(W1_init.astype(np.float32))
    b1 = tf.Variable(b1_init.astype(np.float32))
    W2 = tf.Variable(W2_init.astype(np.float32))
    b2 = tf.Variable(b2_init.astype(np.float32))
    W3 = tf.Variable(W3_init.astype(np.float32))
    b3 = tf.Variable(b3_init.astype(np.float32))
    W4 = tf.Variable(W4_init.astype(np.float32))
    b4 = tf.Variable(b4_init.astype(np.float32))
    W5 = tf.Variable(W5_init.astype(np.float32))
    b5 = tf.Variable(b5_init.astype(np.float32))
    W6 = tf.Variable(W6_init.astype(np.float32))
    b6 = tf.Variable(b6_init.astype(np.float32))

    Z1 = tf.nn.relu(tf.matmul(X_ph, W1) + b1)
    Z2 = tf.nn.relu(tf.matmul(Z1, W2) + b2)
    Z3 = tf.nn.relu(tf.matmul(Z2, W3) + b3)
    Z4 = tf.nn.relu(tf.matmul(Z3, W4) + b4)
    Z5 = tf.nn.relu(tf.matmul(Z4, W5) + b5)
    y_meta = tf.matmul(Z5, W6) + b6

    loss = tf.nn.softmax_cross_entropy_with_logits(logits=y_meta, labels=y_ph)
    reg_l2 = tf.nn.l2_loss(W1) + tf.nn.l2_loss(W2) + tf.nn.l2_loss(W3) + tf.nn.l2_loss(W4) + tf.nn.l2_loss(W5) + tf.nn.l2_loss(W6)
    cost = tf.reduce_mean(loss + (reg_l2*beta))

    optimizer = tf.train.RMSPropOptimizer(learning_rate, decay=0.99, momentum=0.9)
    # optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    # optimizer = tf.train.AdamOptimizer(learning_rate)
    train = optimizer.minimize(cost)
    predict_op = tf.argmax(y_meta, axis=1)
    

    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)
        
        

        for i in range(epochs):
            print("-"*50, "epoch: {}".format(i), "-"*50)
            X_train, y_train = shuffle(X_train, y_train)
            # rand_ind = np.random.randint(len(X_train),size=batch_size)
            
            T_train = np.zeros((X_train_len, n_classes))
            for t in range(X_train_len):
                T_train[t, y_train[t]] = 1
                    
            for j in range(n_batches):
                
                X_batch = X_train[j*batch_size:(j*batch_size) + batch_size]
                Y_batch = T_train[j*batch_size:(j*batch_size) + batch_size]
                sess.run(train, feed_dict={X_ph: X_batch, y_ph: Y_batch})
                
                if(j % 1000 == 0):
                    train_pred = sess.run(predict_op, feed_dict={X_ph: X_train})
                    test_pred = sess.run(predict_op, feed_dict={X_ph: X_test})
                    print("Classification Rate - Train: {}\tTest: {}".format(accuracy_score(y_train, train_pred),
                                                    accuracy_score(y_test, test_pred)))
            # del T_train



def age_category(row):
    if(row <= 40):
        return 'age_1'
    elif(row > 40 and row <= 60):
        return 'age_2'
    else:
        return 'age_3'


if __name__=='__main__':
    main()