import warnings
warnings.filterwarnings('ignore')


import tensorflow.compat.v1 as tf
import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score


def main():
    dataset = datasets.load_iris()
    X = dataset.data
    y = dataset.target.reshape((X.shape[0], 1))
    y=y.ravel()

    X,y = shuffle(X, y)

    normalized_X = (X - X.mean(axis=0)) / X.std(axis=0)

    assert (normalized_X.shape == X.shape)


    n_features = normalized_X.shape[1]
    n_obs = normalized_X.shape[0]
    n_hidden_layer_1 = 5
    n_hidden_layer_2 = 5
    n_classes = 3


    T = np.zeros((n_obs, n_classes))
    for i in range(n_obs):
        T[i, y[i]] = 1



    learning_rate = 0.001
    batch_size = 12
    batches = 10000


    W1_init = np.random.randn(n_features, n_hidden_layer_1)
    b1_init = np.zeros(n_hidden_layer_1)
    W2_init = np.random.randn(n_hidden_layer_1, n_hidden_layer_2)
    b2_init = np.zeros(n_hidden_layer_2)
    W3_init = np.random.randn(n_hidden_layer_2, n_classes)
    b3_init = np.zeros(n_classes)
    
    #Variables and Placeholders
    X_ph = tf.placeholder(tf.float32, shape=(None, n_features), name='Xph')
    y_ph = tf.placeholder(tf.float32, shape=(None, n_classes), name='yph')

    W1 = tf.Variable(W1_init.astype(np.float32))
    b1 = tf.Variable(b1_init.astype(np.float32))
    W2 = tf.Variable(W2_init.astype(np.float32))
    b2 = tf.Variable(b2_init.astype(np.float32))
    W3 = tf.Variable(W3_init.astype(np.float32))
    b3 = tf.Variable(b3_init.astype(np.float32))

    Z1 = tf.nn.relu(tf.matmul(X_ph, W1) + b1)
    Z2 = tf.nn.relu(tf.matmul(Z1, W2) + b2)
    y_meta = tf.matmul(Z2, W3) + b3


    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=y_meta, labels=y_ph))

    optimizer = tf.train.RMSPropOptimizer(learning_rate, decay=0.99, momentum=0.9)
    train = optimizer.minimize(cost)
    predict_op = tf.argmax(y_meta, axis=1)

    init = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(init)

        for i in range(batches):
            rand_ind = np.random.randint(len(normalized_X),size=batch_size)
            sess.run(train, feed_dict={X_ph: normalized_X[rand_ind], y_ph: T[rand_ind]})
            
            if(i % 1000 == 0):
                prediction = sess.run(predict_op, feed_dict={X_ph: normalized_X})
                print("Classification Rate: {}".format(accuracy_score(y, prediction)))


if __name__=='__main__':
    main()