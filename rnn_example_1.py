import numpy as np
import tensorflow as tf

n_inputs = 4
n_neurons = 5
n_timesteps = 4

X_batch = np.array([
        [[0, 1, 2, 5], [9, 8, 7, 4], [9, 8, 7, 4], [9, 8, 7, 4]], # Batch 1
        [[3, 4, 5, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]], # Batch 2
        [[6, 7, 8, 5], [6, 5, 4, 2], [6, 5, 4, 2], [6, 5, 4, 2]], # Batch 3
        [[6, 7, 8, 5], [9, 8, 7, 4], [3, 4, 5, 2], [3, 4, 5, 2]], # Batch 4
        [[0, 1, 2, 5], [9, 8, 7, 4], [9, 8, 7, 4], [9, 8, 7, 4]]  # Batch 5
    ])
seq_length_batch = np.array([1, 2, 3, 3, 4])
X = tf.placeholder(tf.float32, shape=[None, n_timesteps, n_inputs])
seq_length = tf.placeholder(tf.int32, [None])

#Graph
basic_rnn_cell = tf.contrib.rnn.BasicRNNCell(num_units = n_neurons)
outputs, state = tf.nn.dynamic_rnn(basic_rnn_cell, X, sequence_length = seq_length, dtype=tf.float32)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    outputs_val, state_val = sess.run([outputs, state], feed_dict={X: X_batch, seq_length: seq_length_batch})

#State gives the final value. Shape -> (No. of Sequences/Batches, size of neuron). It doesn't consider the the zero vector. 
#Example output[2][3] -> zero vector(because max timestep=3). So State prints output[2][2] instead.
print(state_val)

print("-"*100)
#Prints the output for each batch/sequence. And for each batch, the value at each timestep is printed.
print(outputs_val)

print("-"*100)
#Shape -> (Batch Size, timesteps, n_neurons)
print(outputs_val.shape)

