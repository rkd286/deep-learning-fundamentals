import tensorflow.compat.v1 as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def main():
    df = pd.read_csv('AgeVsExp.csv')
    X_data = df['YearsExperience'].values
    Y_data = df['Salary'].values

    m = tf.Variable(tf.random_normal([1]))
    b = tf.Variable(tf.random_normal([1]))

    #y = mx + b
    error = 0
    for x,y in zip(X_data, Y_data):
        y_hat = m*x + b
        
        error += (y - y_hat)**2


    optimizer = tf.train.GradientDescentOptimizer(learning_rate = 0.0001)
    train = optimizer.minimize(error)

    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)
        epochs = 1024
        
        for i in range(epochs):
            sess.run(train)
        
        final_slope, final_intercept = sess.run([m, b])
    
    
    Y_pred = final_slope*X_data + final_intercept
    plt.scatter(X_data, Y_data)
    plt.plot(X_data, Y_pred, 'r')
    plt.show()


if __name__=='__main__':
    main()
