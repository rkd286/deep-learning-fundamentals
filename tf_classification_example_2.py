import warnings
warnings.filterwarnings('ignore')


import tensorflow.compat.v1 as tf
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

def main():
    df = pd.read_csv('pima-indians-diabetes.csv')
    print(df.columns)
    #Standardization of Features
    numerical_feature_cols = ['Number_pregnant', 'Glucose_concentration', 'Blood_pressure', 'Triceps',
       'Insulin', 'BMI', 'Pedigree', 'Age']
    df[numerical_feature_cols] = df[numerical_feature_cols].apply(lambda x: (x - x.mean()) / x.std(), axis=0)
    df = pd.get_dummies(df, columns=['Group'], drop_first=True)

    normalized_X = df.drop('Class', axis=1).as_matrix()
    y = df['Class'].values

    n_features = normalized_X.shape[1]
    n_obs = normalized_X.shape[0]
    n_classes = 2

    X_train, X_test, y_train, y_test = train_test_split(normalized_X, y, test_size=0.30)

    T_train = np.zeros((len(X_train), n_classes))
    for i in range(len(X_train)):
        T_train[i, y_train[i]] = 1

    T_test = np.zeros((len(X_test), n_classes))
    for i in range(len(X_test)):
        T_test[i, y_test[i]] = 1
    
    T = np.zeros((n_obs, n_classes))
    for i in range(n_obs):
        T[i, y[i]] = 1



    n_hidden_layer_1 = 32
    n_hidden_layer_2 = 32
    n_hidden_layer_3 = 32
    n_hidden_layer_4 = 32
    n_hidden_layer_5 = 32
    n_hidden_layer_6 = 32
   

    learning_rate = 0.000001
    batch_size = 64
    batches = 1000000
    beta = 0.01

    W1_init = np.random.randn(n_features, n_hidden_layer_1)
    b1_init = np.zeros(n_hidden_layer_1)
    W2_init = np.random.randn(n_hidden_layer_1, n_hidden_layer_2)
    b2_init = np.zeros(n_hidden_layer_2)
    W3_init = np.random.randn(n_hidden_layer_2, n_hidden_layer_3)
    b3_init = np.zeros(n_hidden_layer_3)
    W4_init = np.random.randn(n_hidden_layer_3, n_hidden_layer_4)
    b4_init = np.zeros(n_hidden_layer_4)
    W5_init = np.random.randn(n_hidden_layer_4, n_hidden_layer_5)
    b5_init = np.zeros(n_hidden_layer_5)
    W6_init = np.random.randn(n_hidden_layer_5, n_classes)
    b6_init = np.zeros(n_classes)

    #Variables and Placeholders
    X_ph = tf.placeholder(tf.float32, shape=(None, n_features), name='X_ph')
    y_ph = tf.placeholder(tf.float32, shape=(None, n_classes), name='y_ph')

    W1 = tf.Variable(W1_init.astype(np.float32))
    b1 = tf.Variable(b1_init.astype(np.float32))
    W2 = tf.Variable(W2_init.astype(np.float32))
    b2 = tf.Variable(b2_init.astype(np.float32))
    W3 = tf.Variable(W3_init.astype(np.float32))
    b3 = tf.Variable(b3_init.astype(np.float32))
    W4 = tf.Variable(W4_init.astype(np.float32))
    b4 = tf.Variable(b4_init.astype(np.float32))
    W5 = tf.Variable(W5_init.astype(np.float32))
    b5 = tf.Variable(b5_init.astype(np.float32))
    W6 = tf.Variable(W6_init.astype(np.float32))
    b6 = tf.Variable(b6_init.astype(np.float32))

    Z1 = tf.nn.relu(tf.matmul(X_ph, W1) + b1)
    Z2 = tf.nn.relu(tf.matmul(Z1, W2) + b2)
    Z3 = tf.nn.relu(tf.matmul(Z2, W3) + b3)
    Z4 = tf.nn.relu(tf.matmul(Z3, W4) + b4)
    Z5 = tf.nn.relu(tf.matmul(Z4, W5) + b5)
    y_meta = tf.matmul(Z5, W6) + b6

    loss = tf.nn.softmax_cross_entropy_with_logits(logits=y_meta, labels=y_ph)
    reg_l2 = tf.nn.l2_loss(W1) + tf.nn.l2_loss(W2) + tf.nn.l2_loss(W3) + tf.nn.l2_loss(W4) + tf.nn.l2_loss(W5) + tf.nn.l2_loss(W6)
    cost = tf.reduce_mean(loss + (reg_l2 * beta))

    optimizer = tf.train.RMSPropOptimizer(learning_rate, decay=0.99, momentum=0.9)
    # optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    train = optimizer.minimize(cost)
    predict_op = tf.argmax(y_meta, axis=1)

    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)

        for i in range(batches):
            rand_ind = np.random.randint(len(X_train),size=batch_size)
            sess.run(train, feed_dict={X_ph: X_train[rand_ind], y_ph: T_train[rand_ind]})
            
            if(i % 1000 == 0):
                train_pred = sess.run(predict_op, feed_dict={X_ph: X_train})
                test_pred = sess.run(predict_op, feed_dict={X_ph: X_test})
                print("Classification Rate - Train: {}\tTest: {}".format(accuracy_score(y_train, train_pred),
                                                 accuracy_score(y_test, test_pred)))

if __name__=='__main__':
    main()