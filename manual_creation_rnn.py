import numpy as np
import tensorflow as tf


n_inputs = 2
n_neurons = 3

#Placeholders
x0 = tf.placeholder(tf.float32, shape=[None, n_inputs])
x1 = tf.placeholder(tf.float32, shape=[None, n_inputs])

#Variables
Wx = tf.Variable(tf.random_normal(shape=[n_inputs, n_neurons]))
Wy = tf.Variable(tf.random_normal(shape=[n_neurons, n_neurons]))
b = tf.Variable(tf.zeros(shape=[1, n_neurons]))

#Graph
y0 = tf.tanh(tf.matmul(x0, Wx) + b)
y1 = tf.tanh(tf.matmul(y0, Wy) + tf.matmul(x1, Wx) + b)

#Session
init = tf.global_variables_initializer()

x0_batch = np.array([[1,2], [2,3], [3,4]])
x1_batch = np.array([[2,4], [4,6], [6,8]])

with tf.Session() as sess:
    sess.run(init)
    y0_out, y1_out = sess.run([y0, y1], feed_dict={x0: x0_batch, x1: x1_batch})

print(y0_out)
print(y1_out)

