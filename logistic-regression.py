import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.linear_model import LogisticRegression

dataset = datasets.load_iris()
X = dataset.data
y = pd.read_csv('response.csv', header=None)[0].values.reshape((X.shape[0], 1))

# Add ones
X = np.append(X, np.ones((X.shape[0], 1)), axis = 1)

# Initialize Weights
weights = np.random.rand(X.shape[1], 1) / 1000

z = X.dot(weights)

def sigmoid(z):
    return 1/(1 + np.exp(-z))

Y = sigmoid(z)

def cross_entropy(y, Y):
    J = 0
    for i in range(len(y)):
        if(y[i] == 1):
            J -= np.log(Y[i])
        else:
            J -= np.log(1-Y[i])
        
    return J


learning_rate = 0.01

for i in range(1000):
    if(i%100) == 0:
        print("Cross-Entropy Iteration: {}, Value: {}".format(i, cross_entropy(y, Y)[0 ]))
    
    weights += learning_rate*(X.T.dot(y - Y) - 0.1*weights)
    Y = sigmoid(z = X.dot(weights))

print("\nFinal Coefficeints: {}".format(weights))
