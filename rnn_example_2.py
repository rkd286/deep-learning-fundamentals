
import warnings
warnings.filterwarnings('ignore')

import numpy as np
import pandas as pd

import tensorflow as tf
from tensorflow.contrib import rnn

from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score

def main():

    df = pd.read_csv('./data/creditcard.csv', skiprows=[0], header=None)
    
    #Drop Sequence Column - Split into features and labels
    features = df.iloc[:, 1:-1].values
    labels = df.iloc[:, -1].values
    # X = tf.random_normal([None, 29])
    X_train, X_test, y_train, y_test = train_test_split(features, labels, shuffle=False, test_size=0.20)

    #Hyperparams
    epochs = 10
    n_classes = 1
    n_neurons = 200
    n_features = 29
    batch_size = 32

    learning_rate = 0.01

    X_ph = tf.placeholder(tf.float32, [None, n_features])
    y_ph = tf.placeholder(tf.float32, [None])

    logits = rnn_model(n_classes, n_neurons, n_features, X_ph)
    train_rnn_model(logits, X_ph, y_ph, epochs, learning_rate, batch_size, X_train, X_test, y_train, y_test)
    

def rnn_model(n_classes, n_neurons, n_features, X_ph):
    
    
    output_layer = {'weights': tf.random_normal([n_neurons, n_classes]), 'bias': tf.random_normal([n_classes])}

    X = tf.split(X_ph, n_features, axis = 1)
    print(X)

    lstm_cell = rnn.BasicLSTMCell(num_units=n_neurons)

    outputs, state = rnn.static_rnn(lstm_cell, X, dtype=tf.float32)

    output = tf.matmul(outputs[-1], output_layer['weights']) + output_layer['bias']

    return output

def train_rnn_model(logits, X_ph, y_ph, epochs, learning_rate, batch_size, X_train, X_test, y_train, y_test):

    # logit = train_rnn_model()
    logits = tf.reshape(logits, [-1])

    cost = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels = y_ph))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train = optimizer.minimize(cost)

    init = tf.global_variables_initializer()
    local = tf.local_variables_initializer()

    with tf.Session() as sess:

        sess.run(init)
        sess.run(local)

        for epoch in range(epochs):
            print("-"*50, "epoch: {}".format(epoch), "-"*50)
            loss = 0

            # X_train, y_train = shuffle(X_train, y_train)

            for j in range(len(X_train) // batch_size):
                X_batch = X_train[j*batch_size:j*batch_size + batch_size]
                Y_batch = y_train[j*batch_size:j*batch_size + batch_size]

                _, l = sess.run([train, cost], feed_dict={X_ph: X_batch, y_ph: Y_batch})
                
                loss += l
                
                if(j%1000 == 0):
                    predictions = (tf.sigmoid(logits).eval({X_ph: np.array(X_test)})) > 0.5
                    train_pred = (tf.sigmoid(logits).eval({X_ph: np.array(X_train)})) > 0.5
                    # predictions = tf.round(tf.nn.sigmoid(logits)).eval({X_ph: np.array(X_test)})
                    # print(predictions[:10], y_test[:10])

                    print("Loss: {}\tTrain: {}\tTest: {}".format(loss, accuracy_score(np.array(y_train), train_pred),accuracy_score(np.array(y_test), predictions)))
                

                

            
if __name__ == '__main__':
    main()