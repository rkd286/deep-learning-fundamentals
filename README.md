# Deep Learning Fundamentals
* Neural Networks Prerequisites - Linear Regression and Logistic Regression using Gradient Descent and L2 Regularization from scratch.
* Feed Forward and Backprop from scratch using numpy.(iris dataset)
* Scikit Learn's MLP Classifier on iris dataset.
* Basic ANN with Tensorflow.
* Basic RNN with Tensorflow.