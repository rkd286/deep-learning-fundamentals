import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.utils import shuffle

import matplotlib.pyplot as plt

def main():
    dataset = datasets.load_iris()
    X = dataset.data
    y = dataset.target.reshape((X.shape[0], 1))
    y=y.ravel()

    X,y = shuffle(X, y)
    
    n_features = X.shape[1]
    n_obs = X.shape[0]
    hidden_nodes = 6
    n_classes = 3


    # One-hot encoding for the targets
    T = np.zeros((n_obs, n_classes))
    for i in range(n_obs):
        T[i, y[i]] = 1
        

    # Initialize Weights
    W1 = np.random.randn(n_features, hidden_nodes) / 1000
    b1 = np.zeros(hidden_nodes)
    W2 = np.random.randn(hidden_nodes, n_classes) / 1000
    b2 = np.zeros(n_classes)

    learning_rate = 0.00001
    costs = []

    for epoch in range(300000):
        output, hidden = feed_forward(X, W1, b1, W2, b2)

        if(epoch % 1000 == 0):
            c = cost(T, output)
            P = np.argmax(output, axis=1)
            rate = classification_rate(y, P)
            print("Cost: {}\tClasification Rate: {}".format(c, rate))
            costs.append(c)

        W2 += learning_rate * derivative_W2(hidden, T, output)
        b2 += learning_rate * derivative_b2(T, output)

        W1 += learning_rate * derivative_W1(X, hidden, T, output, W2)
        b1 += learning_rate * derivative_b1(T, output, W2, hidden)


    plt.plot(costs)
    plt.show()


def sigmoid(z):
    return 1/(1 + np.exp(-z))

def softmax(a):
    expA = np.exp(a)
    return expA/expA.sum(axis=1, keepdims=True)

def feed_forward(X, W1, b1, W2, b2):
    # Z = sigmoid(X.dot(W1) + b1)
    Z = np.tanh(X.dot(W1) + b1)
    A = Z.dot(W2) + b2

    #Softmax
    return softmax(A), Z

def derivative_W2(Z, T, Y):
    return Z.T.dot(T - Y)

def derivative_b2(T, Y):
    return (T - Y).sum(axis=0)

def derivative_W1(X, Z, T, Y, W2):
    # term1 = (T - Y).dot(W2.T)
    # term2 = np.multiply(term1, np.multiply(Z @ (1 - Z)))
    dZ = (T - Y).dot(W2.T) * Z * (1 - Z)
    return X.T.dot(dZ)

def derivative_b1(T, Y, W2, Z):
    return ((T - Y).dot(W2.T) * Z * (1 - Z)).sum(axis=0)

def cost(T, y):
    tot = T * np.log(y)
    return tot.sum()

def classification_rate(y, P):
    n_correct = 0
    n_total = len(y)

    for i in range(n_total):
        if(y[i] == P[i]):
            n_correct += 1
    
    return (n_correct/n_total)



if __name__=='__main__':
    main()